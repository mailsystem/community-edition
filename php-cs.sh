#!/bin/bash

rm -rf ./checkstyle.xml

phpcs --encoding=utf8 --extensions=php --standard=./rulesetCS.xml --report=checkstyle --report-file=./checkstyle.xml ./Bundle
# phpcs --encoding=utf8 --extensions=php --standard=PSR2 --report=checkstyle --report-file=./checkstyle.xml ./Bundle/LetterBundle