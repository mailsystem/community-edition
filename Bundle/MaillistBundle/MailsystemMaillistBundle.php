<?php
/**
 * File : MailsystemMaillistBundle.php
 */
namespace Mailsystem\Bundle\MaillistBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class MailsystemMaillistBundle
 *
 * @package Mailsystem\Bundle\MaillistBundle
 */
class MailsystemMaillistBundle extends Bundle
{
}
