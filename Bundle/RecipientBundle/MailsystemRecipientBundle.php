<?php
/**
 * File : MailsystemRecipientBundle.php
 */
namespace Mailsystem\Bundle\RecipientBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class MailsystemRecipientBundle
 *
 * @package Mailsystem\Bundle\RecipientBundle
 */
class MailsystemRecipientBundle extends Bundle
{
}
